const input1 = document.querySelector(".input-1"); 
const input2 = document.querySelector(".input-2"); 
const showIcon1 = document.getElementById("showIcon1");
const showIcon2 = document.getElementById("showIcon2");
const hideIcon1 = document.getElementById("hideIcon1");
const hideIcon2 = document.getElementById("hideIcon2");
const submit = document.querySelector('.btn');
const form = document.querySelector('.password-form');
const errorMessage = document.getElementById('error-message');

form.addEventListener('click', (event) => {
    // const target = event.target;
    console.log(target);
    if (target.id === 'showIcon1') {
        input1.type = "text";
        showIcon1.classList.remove("fa-eye"); 
        showIcon1.classList.add("fa-eye-slash");
        showIcon1.style.display = "none";
        hideIcon1.style.display = "block";
    } else if (target.id === 'hideIcon1') {
        input1.type = "password";
        hideIcon1.classList.remove("fa-eye-slash"); 
        hideIcon1.classList.add("fa-eye");
        showIcon1.style.display = "block";
        hideIcon1.style.display = "none";
    } else if (target.id === 'showIcon2') {
        input2.type = "text";
        showIcon2.classList.remove("fa-eye"); 
        showIcon2.classList.add("fa-eye-slash");
        showIcon2.style.display = "none";
        hideIcon2.style.display = "block";
    } else if (target.id === 'hideIcon2') {
        input2.type = "password";
        hideIcon2.classList.remove("fa-eye-slash"); 
        hideIcon2.classList.add("fa-eye");
        showIcon2.style.display = "block";
        hideIcon2.style.display = "none";
    } else if (target.classList.contains('btn')) {
        if (input1.value === input2.value) {
            input1.value = '';
            input2.value = '';
            errorMessage.textContent = '';
            alert('You are welcome');            
        } else {
            errorMessage.textContent = 'Потрібно ввести однакові значення';
        }
    }
});
