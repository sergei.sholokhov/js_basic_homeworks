// Завдання

// Доповнити функцію createNewUser() методами підрахунку віку користувача та його паролем. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Візьміть виконане домашнє завдання номер 5 (створена вами функція createNewUser()) і доповніть її наступним функціоналом:
// При виклику функція повинна запитати дату народження (текст у форматі dd.mm.yyyy) і зберегти її в полі birthday.
// Створити метод getAge() який повертатиме скільки користувачеві років.
// Створити метод getPassword(), який повертатиме першу літеру імені користувача у верхньому регістрі, з'єднану з прізвищем (у нижньому регістрі) та роком народження. (наприклад, Ivan Kravchenko 13.03.1992 → Ikravchenko1992.
// Вивести в консоль результат роботи функції createNewUser(), а також функцій getAge() та getPassword() створеного об'єкта.


function createNewUser() {

    let firstName = prompt('Enter your First Name');
    let lastName = prompt('Enter your Last Name');
    let birthdaySrting = prompt('Enter your Date of Birth dd.mm.yyyy');

    const birthSplit = birthdaySrting.split(".");
    const birthday = new Date(birthSplit[2], birthSplit[1] - 1, birthSplit[0]);

    let newUser = {
        firstName,
        lastName,
        birthday,

        getLogin() {
            let login = (this.firstName.charAt(0) + this.lastName).toLowerCase();
            return login;
        },
        getAge() {
            let today = new Date();
            let age = Math.floor((today - this.birthday) / 31536000000);
            return age;
        },
        getPassword() {
            let pass = `${this.firstName.charAt(0).toUpperCase()}${this.lastName.toLowerCase()}${this.birthday.getFullYear()}`;
            return pass;
        },
    }

    console.log(newUser.getAge());
    console.log(newUser.getPassword());

    return newUser;
}

console.log(createNewUser());


// Теоретичні питання:

// Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
// - Додавання зворотного слеша "\" перед спецсимволами дозволяє їх використовувати як звичайні символи

// Які засоби оголошення функцій ви знаєте?
// - Function Declaration 
// function (){}
// - Function Expression 
// let someVar = function (){}
// - Named Function Expression
// let someVariable = function someName (){}
// - Arrow function
// let someName = () => {} , але можна і без фігурних дужок, якщо тіло функції просте, обмежується одним виразом

// Що таке hoisting, як він працює для змінних та функцій?
// - JavaScript вміє використовувати/викликати змінні та функції до того, як вони були оголошені, це пов'язано з тим, як вони потрапляють у пам'ять у процесі
// компіляції, можна провести асоціацію, ніби вони потрапляють на початок коду, хоча це не зовсім так