// 1. дз потрібно здавати посиланням на репозиторій, а не архівом.
// 2. функція має приймати два аргументи – масив і опціональний другий аргумент parent – DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// – дефолтного значення у вас не має для другого аргументу
// – в функції ви не працюєте з аргументами функції, а берете дані із глобальної області.
// 3. навіщо у вас в функції два шматка коду, який робить одне й те саме?




const arr = [1, '2', true, 'Dog', 'Cat', ['Tomato', 'Potato', 'Pepper']];
const parent = document.body;

const myFunction = (array, parentBlock) => {
    const timer = document.createElement('div');
    parent.appendChild(timer);

    const createList = (items) => {
        const list = document.createElement('ul');
        
        items.map((item) => {
            const listItem = document.createElement('li');

            if (Array.isArray(item)) {
                listItem.appendChild(createList(item));
            } else {
                listItem.textContent = item;
            }
            list.appendChild(listItem);
        });
        
        return list;
    };

    const updateTimer = (seconds) => {
        timer.textContent = `Seconds: ${seconds}`;
    };

    let seconds = 3;
    updateTimer(seconds);
    const interval = setInterval(() => {
        seconds--;
        updateTimer(seconds);
        if (seconds === 0) {
            clearInterval(interval);
            parentBlock.innerHTML = '';
        }
    }, 1000);
    
    parentBlock.appendChild(timer);
    parentBlock.appendChild(createList(arr));

    // setTimeout(() => {
        // parentBlock.innerHTML = '';
    // }, 3000);
};

myFunction(arr, parent)


// Теоретичні питання

// Опишіть, як можна створити новий HTML тег на сторінці.
// -  метод createElement()

// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// - targetElement.insertAdjacentHTML(position, text);
// position - визначає позицію елемента, що додається до елемента, що викликав метод. 
// Повинно відповідати одному з наступних значень (чутливо до регістру):
// 'beforebegin': до самого element (до відкриваючого тегу).
// 'afterbegin': одразу після відкриваючого тегу element (перед першим нащадком).
// 'beforeend': одразу перед закриваючим тегом element (після останнього нащадка).
// 'afterend': після тегу element (після закриваючого тегу). 

// Як можна видалити елемент зі сторінки?
//  - метод element.remove()