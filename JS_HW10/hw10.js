const tabs = document.getElementsByClassName('tabs-title');
let activeButton = document.getElementsByClassName('active');
const text = document.getElementsByClassName('text');
let activeText = document.getElementsByClassName('is-show');

for (let i = 0; i < tabs.length; i++) {
    tabs[i].addEventListener('click', tabSwitch)
};

function tabSwitch() {
    activeButton[0].classList.remove('active');
    this.classList.add('active');
    activeText[0].classList.remove('is-show');
    const arrayTabs = Array.prototype.slice.call(tabs);
    // console.log(arrayTabs);
    const index = arrayTabs.indexOf(this);
    document.getElementsByClassName('text')[index].classList.add('is-show');
};


