let student = {
    name: "",
    lastName: "",
    subjects: {},
};

student.name = prompt("Введить імʼя:");
student.lastName = prompt("Введить призвіще:");

while (true) {
    let subject = prompt("Введить назву предмета:");

    if (subject === null) {
        break;
    }

    let grade = parseFloat(prompt(`Введить оцінку з предмету ${subject}:`));

    student.subjects[subject] = grade;
}

let poorGrades = 0;
for (let subject in student.subjects) {
    if (student.subjects[subject] < 4) {
        poorGrades++;
    }
}

let totalGrade = 0;
let numSubjects = 0;
for (let subject in student.subjects) {
    totalGrade += student.subjects[subject];
    numSubjects++;
}

let averageGrade = totalGrade / numSubjects;

if (poorGrades === 0) {
    alert("Студент переведено на наступний курс");
} else {
    alert(`Студент не впорався з курсом, має ${poorGrades} поганих оцінок.`);
}

if (averageGrade > 7) {
    alert(`Середній бал: ${averageGrade}. Студенту призначено стипендію.`);
}

console.log(`Середній бал: ${averageGrade}`);
console.log(student);