function setTheme(themeName) {
    localStorage.setItem('theme', themeName);
    const currentTheme = localStorage.getItem('theme');
    document.body.setAttribute('data-theme', currentTheme);
}

function loadTheme() {
    const savedTheme = localStorage.getItem('theme');
    if (savedTheme) {
      setTheme(savedTheme);
  } else {
      setTheme('original');
  }
}

function toggleTheme() {
    const currentTheme = localStorage.getItem('theme');
    if (currentTheme === 'original') {
        setTheme('dark');
    } else {
        setTheme('original');
    }
    // console.log(localStorage);
}

loadTheme();

document.getElementById('toggle').addEventListener('click', toggleTheme);