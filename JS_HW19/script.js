function daysBeforeDeadline(userScores, taskList, projectDeadline) {
  // Calculate the total capacity of the team based on user scores
  const teamCapacity = userScores.reduce((acc, score) => acc + score, 0);

  console.log(teamCapacity);
  
  // Calculate the total effort required for all tasks based on story points
  const totalEffortRequired = taskList.reduce((acc, storyPoints) => acc + storyPoints, 0);

  console.log(totalEffortRequired);
  
  // Calculate the number of work hours available before the deadline
  const currentDate = new Date();
  const millisecondsPerDay = 24 * 60 * 60 * 1000; // Number of milliseconds in a day
  const workDaysLeft = Math.floor((projectDeadline - currentDate) / millisecondsPerDay / 7) * 5; // Assuming 5 working days per week
  const workHoursLeft = workDaysLeft * 8;

  console.log(workHoursLeft);

  const totalDays = totalEffortRequired / (teamCapacity * 8); // Assuming 8 working hours per day
  
  // Calculate the number of days remaining before the deadline
  const daysRemaining = workHoursLeft / 8; // Assuming 8 working hours per day

  console.log(`${daysRemaining} дней займет у комнады на всю работу`);
  
  return daysRemaining;
}

// Example usage:
const userScores = [85, 92, 78, 65, 90];
const taskList = [3, 5, 2, 8, 4];
const projectDeadline = new Date('2023-12-31');

const daysRemaining = daysBeforeDeadline(userScores, taskList, projectDeadline);

console.log(`There are approximately ${daysRemaining.toFixed(2)} days remaining before the deadline.`);































