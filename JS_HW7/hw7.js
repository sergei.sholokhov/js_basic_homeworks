let arr = [1, '2', true, undefined, { 1: 'une', '2': 2, 'bool': true }, 0, 'super string'];
let dataType = ['number', 'undefined', 'object', 'boolean'];

let filterBy = (arr, dataType) => {
    let newArr = arr.filter(element => !dataType.includes(typeof element));
    
    return newArr
}

console.log(filterBy(arr, dataType));