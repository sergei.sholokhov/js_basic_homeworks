// ------------------------------ Services ------------------------------------ //

const serviceTabButtons = document.querySelectorAll('.serviceButton');
const serviceTabContents = document.querySelectorAll('.service-container');

serviceTabButtons.forEach(button => {
    button.addEventListener('click', () => {
        const tabId = button.getAttribute('data-tab');

        serviceTabContents.forEach(content => {
            content.classList.remove('service-content-active');
        });

        const selectedContent = document.getElementById(tabId + '-content');
        if (selectedContent) {
            selectedContent.classList.add('service-content-active');
        }

        serviceTabButtons.forEach(btn => {
            btn.classList.remove('service-button-active');
        });

        button.classList.add('service-button-active');
    });
});


// ------------------------------ WORKS ------------------------------------ //

const images = [
    { src: 'assets/graphic_design/graphic-design1.jpg', tag: 'graphic design' },
    { src: 'assets/graphic_design/graphic-design2.jpg', tag: 'graphic design' },
    { src: 'assets/graphic_design/graphic-design3.jpg', tag: 'graphic design' },
    { src: 'assets/graphic_design/graphic-design4.jpg', tag: 'graphic design' },
    { src: 'assets/graphic_design/graphic-design5.jpg', tag: 'graphic design' },
    { src: 'assets/graphic_design/graphic-design6.jpg', tag: 'graphic design' },
    { src: 'assets/graphic_design/graphic-design7.jpg', tag: 'graphic design' },
    { src: 'assets/graphic_design/graphic-design8.jpg', tag: 'graphic design' },
    { src: 'assets/graphic_design/graphic-design9.jpg', tag: 'graphic design' },
    { src: 'assets/graphic_design/graphic-design10.jpg', tag: 'graphic design' },
    { src: 'assets/graphic_design/graphic-design11.jpg', tag: 'graphic design' },
    { src: 'assets/graphic_design/graphic-design12.jpg', tag: 'graphic design' },
    { src: 'assets/web_design/web-design1.jpg', tag: 'web design' },
    { src: 'assets/web_design/web-design2.jpg', tag: 'web design' },
    { src: 'assets/web_design/web-design3.jpg', tag: 'web design' },
    { src: 'assets/web_design/web-design4.jpg', tag: 'web design' },
    { src: 'assets/web_design/web-design5.jpg', tag: 'web design' },
    { src: 'assets/web_design/web-design6.jpg', tag: 'web design' },
    { src: 'assets/web_design/web-design7.jpg', tag: 'web design' },
    { src: 'assets/landing/landing-page1.jpg', tag: 'landing pages' },
    { src: 'assets/landing/landing-page2.jpg', tag: 'landing pages' },
    { src: 'assets/landing/landing-page3.jpg', tag: 'landing pages' },
    { src: 'assets/landing/landing-page4.jpg', tag: 'landing pages' },
    { src: 'assets/landing/landing-page5.jpg', tag: 'landing pages' },
    { src: 'assets/landing/landing-page6.jpg', tag: 'landing pages' },
    { src: 'assets/landing/landing-page7.jpg', tag: 'landing pages' },
    { src: 'assets/wordpress/wordpress1.jpg', tag: 'wordpress' },
    { src: 'assets/wordpress/wordpress2.jpg', tag: 'wordpress' },
    { src: 'assets/wordpress/wordpress3.jpg', tag: 'wordpress' },
    { src: 'assets/wordpress/wordpress4.jpg', tag: 'wordpress' },
    { src: 'assets/wordpress/wordpress5.jpg', tag: 'wordpress' },
    { src: 'assets/wordpress/wordpress6.jpg', tag: 'wordpress' },
    { src: 'assets/wordpress/wordpress7.jpg', tag: 'wordpress' },
    { src: 'assets/wordpress/wordpress8.jpg', tag: 'wordpress' },
    { src: 'assets/wordpress/wordpress9.jpg', tag: 'wordpress' },
    { src: 'assets/wordpress/wordpress10.jpg', tag: 'wordpress' }
];

const tags = ['web design', 'graphic design', 'landing pages', 'wordpress'];


const tagButtonsContainer = document.getElementById('tagButtons');
const imageGrid = document.getElementById('imageGrid');
const showMoreButton = document.getElementById('showMoreButton');

let currentTag = 'all';
let ItemShow = 12;

// ------- Render tags -----------

function renderTagButtons() {
    tagButtonsContainer.innerHTML = `<li><button class="tagButton active" data-tag="all">All</button></li>`;
    tags.forEach(tag => {
        tagButtonsContainer.innerHTML += `<li><button class="tagButton" data-tag="${tag}">${tag}</button></li>`;
    });
}

// ------- Render grid -----------

function renderImages(tag) {
    imageGrid.innerHTML = '';

    const filteredImages = images.filter(img => tag === 'all' || img.tag === tag)
        .slice(0, ItemShow);
    filteredImages.forEach(image => {
        const imageItem = document.createElement('div');
        imageItem.className = 'imageItem';
        imageItem.innerHTML = `
        <img src="${image.src}" alt="Image ${image.tag}">
        <div class="imageContent">
                        <img src="assets/icons/icons2.svg">
                        <div class="grid1-title">Creative design</div>
                        <div class="grid1-p">Web Design</div>
        </div>
      `;
        imageGrid.appendChild(imageItem);
    });
 
    const displayedImagesCount = filteredImages.length;
    // console.log(displayedImagesCount);
    showMoreButton.style.display = displayedImagesCount < ItemShow ? 'none' : 'flex';  // але так кнопка зникає тільки після ще одного натискання
  }



// ------- On click -----------

tagButtonsContainer.addEventListener('click', event => {
    const clickedButton = event.target.closest('.tagButton');
    if (clickedButton) {
        tagButtonsContainer.querySelectorAll('.tagButton').forEach(btn => btn.classList.remove('active'));
        clickedButton.classList.add('active');
        currentTag = clickedButton.getAttribute('data-tag');
        renderImages(currentTag);
    }
});

// ------- Load more -----------

function loadMoreImages() {
    ItemShow += 12;
    renderImages(currentTag); 
    // showMoreButton.style.display = 'none'; // -  Спрацьовує умова основного завдання - "При її натисканні в секції знизу мають з'явитись ще 12 картинок (зображення можна взяти будь-які). Після цього кнопка зникає."
}

showMoreButton.addEventListener('click', loadMoreImages);

renderTagButtons();
renderImages(currentTag);

// ------------------------------ NEWS ------------------------------------ //

const newsArray = [
    { day: 12, month: 'Feb', img: 'assets/breaking_news/news01.png', title: 'Amazing Blog Post', author: 'admin', comments: 2, link: "https://dan-it.com/" },
    { day: 12, month: 'Feb', img: 'assets/breaking_news/news02.png', title: 'Amazing Blog Post', author: 'admin', comments: 1, link: "https://dan-it.com/" },
    { day: 12, month: 'Feb', img: 'assets/breaking_news/news03.png', title: 'Amazing Blog Post', author: 'username', comments: 2, link: "https://dan-it.com/" },
    { day: 12, month: 'Feb', img: 'assets/breaking_news/news04.png', title: 'Amazing Blog Post', author: 'admin', comments: 2, link: "https://dan-it.com/" },
    { day: 12, month: 'Feb', img: 'assets/breaking_news/news05.png', title: 'Amazing Blog Post', author: 'username', comments: 3, link: "https://dan-it.com/" },
    { day: 12, month: 'Feb', img: 'assets/breaking_news/news06.png', title: 'Amazing Blog Post', author: 'admin', comments: 0, link: "https://dan-it.com/" },
    { day: 12, month: 'Feb', img: 'assets/breaking_news/news07.png', title: 'Amazing Blog Post', author: 'username', comments: 3, link: "https://dan-it.com/" },
    { day: 12, month: 'Feb', img: 'assets/breaking_news/news08.png', title: 'Amazing Blog Post', author: 'admin', comments: 2, link: "https://dan-it.com/" },
]

const newsGrid = document.getElementById("newsGrid");

function renderNewsArray() {
    newsArray.forEach(news => {

        const newsCard = document.createElement("a");  
        newsCard.href = news.link;  
        newsCard.target = "_blank"; 
        newsCard.classList.add("grid-item-news");

        const newsImage = document.createElement("img");
        newsImage.classList.add("grid-photo");
        newsImage.src = news.img;
        newsImage.alt = news.title;

        const newsTitle = document.createElement("div");
        newsTitle.classList.add("grid-title");
        newsTitle.textContent = news.title;

        const newsMeta = document.createElement("div");
        newsMeta.classList.add("grid-meta");
        newsMeta.textContent = "By " + news.author + '\xa0\xa0\xa0\xa0' + "|" + '\xa0\xa0\xa0\xa0' + news.comments + " comments";

        const newsDate = document.createElement("div");
        newsDate.classList.add("grid-date");
        newsDate.innerHTML = news.day + '<br>' + news.month;


        newsCard.appendChild(newsImage);
        newsCard.appendChild(newsDate);
        newsCard.appendChild(newsTitle);
        newsCard.appendChild(newsMeta);

        newsGrid.appendChild(newsCard);
    });
}

renderNewsArray();



// ------------------------------ TESTIMONIALS ------------------------------------ //

const carouselContainer = document.querySelector('.carousel-container');
const slides = document.querySelectorAll('.carousel-slide');
const thumbnails = document.querySelectorAll('.thumbnail');
const arrowLeft = document.querySelector('.arrow-left');
const arrowRight = document.querySelector('.arrow-right');

thumbnails.forEach((thumbnail, index) => {
  thumbnail.addEventListener('click', () => showSlide(index));
});

arrowLeft.addEventListener('click', () => showSlide(currentIndex - 1));
arrowRight.addEventListener('click', () => showSlide(currentIndex + 1));

function showSlide(index) {

    if (index < 0) {
      currentIndex = slides.length - 1;
    } else if (index >= slides.length) {
      currentIndex = 0;
    } else {
      currentIndex = index;
    }
  updateCarousel();
  updateThumbnails();
}

function updateCarousel() {
  const slideWidth = slides[0].offsetWidth;
  carouselContainer.style.transform = `translateX(-${currentIndex * slideWidth}px)`;

  slides.forEach((slide, index) => {
    slide.classList.toggle('active', index === currentIndex);
  });
}

function updateThumbnails() {
  thumbnails.forEach((thumbnail, index) => {
    thumbnail.classList.toggle('active', index === currentIndex);
  });
}

showSlide(0);


