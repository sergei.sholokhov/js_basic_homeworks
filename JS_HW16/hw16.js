function calculateFibonacci(F0, F1, n) { 

    let currentFibonacci;

    if (n === 0) {
        alert(`The ${n} Fibonacci number is: ${F0}`);
        return;
    } else if (n === 1) {
        currentFibonacci = F1;
    } else if (n === 2) {
        currentFibonacci = F1;
    } else if (n > 2) {
        for (let i = 2; i <= n; i++) {
            currentFibonacci = F0 + F1;
            F0 = F1;
            F1 = currentFibonacci;
        }
    } else if (n === -1) {
        alert(`The ${n} Fibonacci number is: ${n}`);
        return;
    } else if (n < -1) {
        for (let i = -2; i >= n; i--) {
            currentFibonacci = F0 - F1;
            F0 = F1;
            F1 = currentFibonacci;
        }
        alert(`The ${n} Fibonacci number is: ${currentFibonacci}`);
        return;
    }

    alert(`The ${n} Fibonacci number is: ${currentFibonacci}`);
}

const F0 = 0;
const F1 = 1;
const n = +(prompt("Enter the Fibonacci serial number (n):"));

calculateFibonacci(F0, F1, n);
