// 1) Знайти всі параграфи на сторінці та встановити колір фону #ff0000
console.log("task1");

const par = document.getElementsByTagName("p");
for (let p of par) {
    p.style.backgroundColor = "#ff0000"; 
    console.log(p);
}

console.log("\n"); 

// 2) Знайти елемент із id="optionsList". Вивести у консоль. Знайти 
// батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони 
// є, і вивести в консоль назви та тип нод.

console.log("task2");

const elId = document.getElementById("optionsList");
console.log(elId);
console.log(elId.parentElement);
for (let childs of elId.children) {
    console.log(childs.nodeName);
    console.log(childs.nodeType);
}

console.log("\n"); 

// 3) Встановіть в якості контента елемента з класом testParagraph наступний 
// параграф - <p>This is a paragraph<p/>
console.log("task3");

console.log(document.querySelector('.testParagraph')); //немає такого класа
// але є такий ID, це помилка в завданні?
const parTest = document.getElementById('testParagraph');
parTest.innerText = "This is a paragraph";
console.log(parTest);

console.log("\n"); 

// 4) Отримати елементи (*помилково достучався до <li> у попередньому виконанні*), вкладені в елемент із класом main-header і 
// вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
console.log("task4");

const elements = document.querySelector('.main-header').children;

console.log(elements);

for (let elem of elements) {
    elem.classList.add("nav-item")
    console.log(elem);
}

console.log("\n"); 

// 5) Знайти всі елементи із класом section-title. Видалити цей клас у цих 
// елементів.  
console.log("task5");

const elClassAll = document.querySelectorAll('.section-title');
// console.log(elClassAll);
for (let el of elClassAll) {
    el.classList.remove('section-title');
    console.log(el);
}


