const images = ["./img/1.jpg", "./img/2.jpg", "./img/3.JPG", "./img/4.png"];
let index = 0;
const div = document.querySelector('.images-wrapper');
const image = document.createElement('img');
image.src = images[index];
image.alt = 'image of game';
image.className = 'image-to-show';
div.appendChild(image);

function displayImage() {
  div.innerHTML = '';
  index++ 

  image.src = images[index];
  div.appendChild(image);

  if (index > 2) index = 0;  
}

function startShow() {
    interval = setInterval(displayImage, 3000);
}

function stopShow() {
      clearInterval(interval);
  }
  
const stopButton = document.createElement('button');
stopButton.textContent = 'Припинити';
stopButton.classList.add('button');
document.body.appendChild(stopButton);
stopButton.addEventListener('click', stopShow);

const continueButton = document.createElement('button');
continueButton.textContent = 'Відновити показ';
continueButton.classList.add('button');
document.body.appendChild(continueButton);
continueButton.addEventListener('click', startShow);

startShow();


// Відповіді на теоретичні питання у файлі readme.md