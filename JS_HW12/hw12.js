// Завдання

// Реалізувати функцію підсвічування клавіш. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

// У файлі index.html лежить розмітка для кнопок.
// Кожна кнопка містить назву клавіші на клавіатурі
// Після натискання вказаних клавіш – та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір – вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.


const buttons = document.querySelectorAll('button.btn');
// console.log(buttons);

document.addEventListener("keydown", (event) => {
    const key = event.key.toUpperCase(); 

    const buttonsArr = Array.from(buttons);
    // console.log(buttonsArr);

    const keyButton = buttonsArr.find(button => button.textContent === key);
    // console.log(keyButton); 

    buttonsArr.forEach(button => {
        button.classList.remove('active')
        // console.log(button);
    });  
    
    if (keyButton) {
        keyButton.classList.add('active');  
    } 
}
);


// Чому для роботи з input не рекомендується використовувати клавіатуру?
// Існує спеціальний event - input, спрацьовуває щоразу при зміні значення, воно краще підходить для відстеження змін